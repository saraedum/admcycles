.. _DRmodule:

Module `DR` (by Aaron Pixton)
=============================

.. automodule:: admcycles.DR
   :members:
   :undoc-members:
   :show-inheritance:

