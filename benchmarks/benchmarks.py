from sage.all import cached_function, PermutationGroup
from admcycles import *
from admcycles.admcycles import pullpushtest, deltapullpush, checkintnum, pullandidentify, pushpullcompat, lambdaintnumcheck

# ASV forks before running a benchmark so all caches are empty when a benchmark
# function is first run. However, asv does not fork between repetitions of a
# benchmark so make sure to always run asv with --quick. We could fix this by
# decorating each benchmark or suite such that it forks again; wouldn't be hard
# if we believe that this makes sense.
@cached_function
def nothing_is_cached():
    return None

class SuiteThatShouldHaveABetterName:
    def setup(self, n=None):
        self.G = PermutationGroup([(1, 2)])
        self.H = HurData(self.G, [self.G[1], self.G[1]])

    def time_something(self):
        assert nothing_is_cached.cache == {}
        nothing_is_cached()
        pullpushtest(2, self.H, 2)

    def time_something_parametrized(self, n):
        assert nothing_is_cached.cache == {}
        nothing_is_cached()
        deltapullpush(2, self.H, n)

    time_something_parametrized.params = [1, 2]
