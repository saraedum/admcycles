# Benchmarks for admcycles

These benchmarks are automatically run by [GitLab CI](../.gitlab-ci.yml) with
[airspeed velocity](https://asv.readthedocs.org). Graphs are generated which
show how each benchmark has evolved over time. To see these graphs, check the
Build Artifacts of the Benchmark job on GitLab.

Note that no graphs are generated if you `from sage.all import *`, see
https://github.com/airspeed-velocity/asv/issues/901.
